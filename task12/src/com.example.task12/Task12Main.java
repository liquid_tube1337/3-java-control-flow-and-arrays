package com.example.task12;

public class Task12Main {
    public static void main(String[] args) {
        //здесь вы можете вручную протестировать ваше решение, вызывая реализуемый метод и смотря результат
        // например вот так:

        int[] arr = {9, 11, 7, 8};
        selectionSort(arr);
        System.out.println(java.util.Arrays.toString(arr));
    }
    static int numMin(int[] arr, int startIndex) {
        int min = Integer.MAX_VALUE;
        int index = 0;
        for (int i=startIndex; i<arr.length; i++){
            if (arr[i] <= min){
                min = arr[i];
                index = i;
            }
        }
        return index;
    }
    static void swap(int[] arr, int startIndex) {
        if (arr == null || arr.length == 0)
            return;
        int minIndex = numMin(arr, startIndex);
        int temp = arr[startIndex];
        arr[startIndex] = arr[minIndex];
        arr[minIndex] = temp;
    }
    static void selectionSort(int[] arr) {
        if (arr == null || arr.length == 0)
            return;
        for (int i=0; i < arr.length; i++){
            swap(arr, i);
        }
    }

}