package com.example.task13;

public class Task13Main {
    public static void main(String[] args) {
        //здесь вы можете вручную протестировать ваше решение, вызывая реализуемый метод и смотря результат
        // например вот так:
        int[] arr = {9, 1100, 7, 8};
        if (arr == null || arr.length ==0)
            return;
        arr = removeMoreThen1000(arr);
        System.out.println(java.util.Arrays.toString(arr));
    }

    static int[] removeMoreThen1000(int[] arr) {
        if (arr == null || arr.length == 0)
            return arr;
        int j = 0;
        for (int i=0; i < arr.length; i++) {
            if (arr[i]>1000){
                int thousandIndex = i;
                int [] newArray = new int[arr.length - 1];
                for (int x=0; x < arr.length; x++) {
                    if (x == thousandIndex) {
                        continue;
                    }
                    newArray[j] = arr[x];
                    j += 1;
                }
                arr = newArray;
            }
        }
        return arr;
    }

}