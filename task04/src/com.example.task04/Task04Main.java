package com.example.task04;

public class Task04Main {
    public static void main(String[] args) {
        //здесь вы можете вручную протестировать ваше решение, вызывая реализуемый метод и смотря результат
        // например вот так:
        from0to10000();
         /**/
    }

    static void from0to10000() {
        int x = 0;
        while (x <= 9999){
            System.out.println(x);
            x += 1;
        }
    }

}