package com.example.task11;

public class Task11Main {
    public static void main(String[] args) {
        //здесь вы можете вручную протестировать ваше решение, вызывая реализуемый метод и смотря результат
        // например вот так:
        /*
        int[] arr = {7, 5, 9};
        swap(arr);
        System.out.println(java.util.Arrays.toString(arr));
         */
    }
    static int numMin(int[] arr) {
        int min = Integer.MAX_VALUE;
        int index = 0;
        for (int i=0; i<arr.length; i++){
            if (arr[i] <= min){
                min = arr[i];
                index = i;
            }
        }
        return index;
    }
    static void swap(int[] arr) {
        if (arr == null || arr.length == 0)
            return;
        int minIndex = numMin(arr);
        int temp = arr[0];
        arr[0] = arr[minIndex];
        arr[minIndex] = temp;
    }

}